package e2e.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class ReceiveTravelerInformationSteps {

    private String requestBody;
    private Response response;

    @Given("My id {string} as a traveler with my new IBAN {string}")
    public void myIdAsATravelerWithMyNewIBAN(String id, String iban) {
        //Java 15 Multiline Feature: Textblock
        requestBody = String.format("""
                {
                    "id": "%s",
                    "iban": "%s"
                }
                """, id, iban);
    }

    @When("I am updating my IBAN")
    public void iAmUpdatingMyIBAN() {
        response = given().baseUri("http://localhost:8080").body(requestBody).contentType(ContentType.JSON)
                .when()
                .put("/traveler");
    }

    @Then("I will receive the new IBAN {string}")
    public void iWillReceiveTheNewIBAN(String iban) {
        response.then().assertThat().statusCode(200)
                .body("iban", equalTo(iban));
    }
}
