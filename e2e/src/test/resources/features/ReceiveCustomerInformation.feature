Feature: Updating traveler information

  Scenario: As a customer is will receive my deposited IBAN.
    Given My id "VeryUniqueId" as a traveler with my new IBAN "DE11 1111 1111 1111 1111 11"
    When I am updating my IBAN
    Then I will receive the new IBAN "DE11 1111 1111 1111 1111 11"