package application;

import core.Traveler;
import core.TravelerInformation;
import core.UpdateTravelerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = TravelerController.class)
class TravelerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UpdateTravelerService updateTravelerService;

    @Test
    void should_receive_traveler_information() throws Exception {
        String testIban = "testIban";
        Traveler traveler = traveler(testIban);
        when(updateTravelerService.updateTraveler(traveler))
                .thenReturn(TravelerInformation.builder().iban(testIban).build());
        mockMvc.perform(put("/traveler").content(String.format("""
                        {
                            "iban": "%s"
                        }
                        """, "testIban")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(".iban").value("testIban"));
    }

    private static Traveler traveler(String testIban) {
        Traveler traveler = new Traveler();
        traveler.setIban(testIban);
        return traveler;
    }
}