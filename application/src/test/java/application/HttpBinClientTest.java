package application;

import feign.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class HttpBinClientTest {
  //Schreibe den Code life im Video.
  @Autowired
  private HttpBinClient httpBinClient;

  @Test
  void testGetRequest() throws IOException {
    Response response = httpBinClient.get();
    assertThat(response.status()).isEqualTo(200);
    System.out.println(new String(response.body().asInputStream().readAllBytes()));
  }
}

