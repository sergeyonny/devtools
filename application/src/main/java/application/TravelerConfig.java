package application;

import core.UpdateTravelerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TravelerConfig {

    @Bean
    public UpdateTravelerService updateTravelerService() {
        return new UpdateTravelerService();
    }
}
