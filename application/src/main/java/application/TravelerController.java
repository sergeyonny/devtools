package application;

import core.Traveler;
import core.TravelerInformation;
import core.UpdateTravelerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TravelerController {

    @Autowired
    private UpdateTravelerService updateTravelerService;

    @PutMapping(value = "traveler")
    public TravelerInformation updateTraveler(@RequestBody Traveler traveler) {
        return updateTravelerService.updateTraveler(traveler);
    }
}
