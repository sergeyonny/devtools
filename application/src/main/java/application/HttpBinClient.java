package application;

import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "httpbinClient", url = "https://httpbin.org")
public interface HttpBinClient {
  @GetMapping("/get")
  Response get();
}
