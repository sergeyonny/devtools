package core;

import lombok.Data;

@Data
public class Traveler {
    String iban;
}
