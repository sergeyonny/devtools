package core;

public class UpdateTravelerService {

    public TravelerInformation updateTraveler(Traveler traveler) {
        return TravelerInformation.builder().iban(traveler.getIban()).build();
    }
}
