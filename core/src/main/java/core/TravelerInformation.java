package core;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TravelerInformation {
    String iban;
}
